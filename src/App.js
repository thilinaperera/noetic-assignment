import React, { Component } from 'react';
import { connect } from 'react-redux';
import './App.css';
import Order from './components/Order';
import Loading from './components/Loading';
import Payment from './components/Payment';


class App extends Component {
    render() {
        return (
            <div className="app">
                <div className="app__body container">
                    <div className="row">
                        <div className="col-lg-10 m-auto">
                            <div className="row">
                                { this.props.loading ? (
                                    [<Order key='Order'/>,<Loading key="Loading"/>]
                                ) : (
                                    [<Order key='Order'/>,<Payment key="Payment"/>]
                                ) }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ payment: { loading } }) => ({
    loading
});
const mapDispatchToProps = (dispatch) => ({});
export default connect(mapStateToProps,mapDispatchToProps)(App)
