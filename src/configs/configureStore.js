import { createStore,applyMiddleware } from 'redux';
import rootReducer from '../reducers/rootReducer';
import { createLogger } from 'redux-logger'

const loggerMiddleware = createLogger();

const configureStore = (preState = {}) => {
    return createStore(rootReducer,preState,applyMiddleware(
        loggerMiddleware
    ))
};
export default configureStore;