import { combineReducers } from 'redux';
import {
    PAYMENT_SUBMITTED
} from '../actions/actions';

const payment = (state = {
    cardData: {},
    loading: false
}, { type, data:cardData }) => {
    switch (type) {
        case PAYMENT_SUBMITTED:
            return Object.assign({},state,{
                cardData,
                loading: true
            });
    }
    return state;
};

const rootReducer = combineReducers({
    payment
});

export default rootReducer;