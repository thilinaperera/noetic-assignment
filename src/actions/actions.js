export const PAYMENT_SUBMITTED = 'PAYMENT_SUBMITTED';

export function paymentSubmitted(data) {
    return {
        type: PAYMENT_SUBMITTED,
        data
    }
}